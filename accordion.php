<?php
/**
 * Plugin Name:     Fantassin — Block Accordion
 * Description:     Add accordions into your content
 * Version:         2.0.5
 * Author:          Fantassin
 * Author URI:      https://www.fantassin.fr/
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     fantassin-block-accordion
 * Domain Path:     /languages
 */

namespace Fantassin\includes;

use Fantassin\Core\WordPress\Plugin\PluginKernel;

class Accordion extends PluginKernel {

	public function getTextDomain(): string {
		return 'fantassin-block-accordion';
	}
}

$plugin = new Accordion( wp_get_environment_type(), WP_DEBUG );
$plugin->load();
