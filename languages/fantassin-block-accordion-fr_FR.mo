��          �      �       H  	   I     S     b     r  "   �      �  1   �  	             .     A     a     i  I  �     �     �       (     0   D  0   u  (   �  	   �     �     �  9        E     M                          	         
                              Accordion Accordion Item Accordion Title Add a title to your accordion Add accordions inside your content Add accordions into your content Add an accordion item inside your accordion block Fantassin Fantassin — Block Accordion Is open by default Let items open even when change Options https://www.fantassin.fr/ Project-Id-Version: Fantassin — Block Accordion
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/accordione
PO-Revision-Date: 2022-01-11 11:31+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: accordion.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
X-Poedit-SearchPathExcluded-1: vendor
X-Poedit-SearchPathExcluded-2: node_modules
 Contenu de l’accordéon Contenu de l’accordéon Contenu de l’accordéon Ajouter du contenu dans votre accordéon Ajouter des accordéons au sein de votre contenu Ajouter des accordéons au sein de votre contenu Ajouter du contenu dans votre accordéon Fantassin Fantassin — Block Accordéon Ouvert par défaut Laisser les accordéons ouverts lors d’une intéraction Options https://www.fantassin.fr/ 