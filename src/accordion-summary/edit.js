import {InnerBlocks, InspectorControls, RichText} from '@wordpress/block-editor';
import {__} from '@wordpress/i18n';
import {Icon, chevronDown} from '@wordpress/icons';
import {FontSizePicker} from "@wordpress/components";
import {Fragment} from "react";

const EditAccordionSummary = ({className, attributes, setAttributes}) => {
  return (
    <div style={{
      marginTop: '-28px',
      paddingBottom: '30px',
      paddingTop: '30px',
      borderBottom: '1px solid #e5e5e5',
    }}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: '0 30px',
        }}
      >
          <RichText
            tagName="summary"
            className={className}
            value={attributes.content}
            onChange={(content) => setAttributes({content})}
            placeholder={ __( 'Add a title', 'fantassin-block-accordion' ) }
          />
        <Icon icon={chevronDown}/>
      </div>
    </div>
  );
};

export default EditAccordionSummary;
