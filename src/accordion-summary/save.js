import {RichText} from "@wordpress/block-editor";
import {useBlockProps} from "@wordpress/block-editor";

const SaveAccordionDetails = ({attributes}) => {
  const blockProps = useBlockProps.save();

  return <RichText.Content {...blockProps} tagName="summary" value={attributes.content}/>
};

export default SaveAccordionDetails;
