const AccordionSummaryAttributes = {
  content: {
    "type": "string",
    "source": 'html',
    "selector": 'summary'
  }
}

export default AccordionSummaryAttributes;
