/**
 * WordPress dependencies
 */
import {registerBlockType} from '@wordpress/blocks';
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import attributes from './attributes';
import edit from './edit';
import save from './save';

/**
 * Register: Accordion Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('fantassin/accordion-summary', {
  title: __('Accordion Title', 'fantassin-block-accordion'),
  description: __(
    'Add a title to your accordion',
    'fantassin-block-accordion'
  ),
  keywords: ['accordion'],
  supports: {
    anchor: true,
    html: false,
    align: ['left', 'right', 'center'],
    typography: {
      fontSize: true
    }
  },
  parent: ['fantassin/accordion-details'],
  category: 'common',
  attributes,
  edit,
  save
});
