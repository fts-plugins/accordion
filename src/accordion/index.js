/**
 * WordPress dependencies
 */
import {registerBlockType} from '@wordpress/blocks';
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import edit from './edit';
import attributes from './attributes';
import save from './save';
import {InnerBlocks, useBlockProps} from "@wordpress/block-editor";
import SaveAccordion from "./save";

/**
 * Register: Accordion Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType('fantassin/accordion', {
  title: __('Accordion', 'fantassin-block-accordion'),
  description: __(
    'Add accordions inside your content',
    'fantassin-block-accordion'
  ),
  keywords: ['accordion'],
  supports: {
    anchor: true,
    html: false,
    align: ['wide', 'full'],
  },
  category: 'common',
  attributes,
  edit,
  save,
  deprecated: [
    {
      attributes: attributes,
      migrate({isOpenAll}) {
        return {
          isOpenAll: !isOpenAll
        };
      },
      save(props) {
        return (
          <div aria-multiselectable={props.attributes.isOpenAll}>
            <InnerBlocks.Content/>
          </div>
        );
      },
    }
  ]
});
