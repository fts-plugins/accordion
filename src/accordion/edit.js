import { InnerBlocks, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow, ToggleControl } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

const AccordionEdit = ( { attributes, setAttributes } ) => {
	const { isOpenAll } = attributes;
	const ALLOWED_BLOCKS = [ 'fantassin/accordion-details' ];
	const TEMPLATE = [ ALLOWED_BLOCKS ];

	const handleCheckboxChange = ( isOpenAll ) => {
		setAttributes( { isOpenAll } );
	};

	return (
		<div className="fantassin-block-accordion block-list-appender">
			<InspectorControls>
				<PanelBody
					title={ __( 'Options', 'fantassin-block-accordion' ) }
				>
					<PanelRow>
						<ToggleControl
							label={ __(
								'Close all opened items when opening a new one',
								'fantassin-block-accordion'
							) }
							checked={ isOpenAll }
							onChange={ handleCheckboxChange }
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>

			<InnerBlocks
				template={ TEMPLATE }
        templateLock={ false }
				allowedBlocks={ ALLOWED_BLOCKS }
			/>
		</div>
	);
};

export default AccordionEdit;
