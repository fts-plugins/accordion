import { InnerBlocks } from '@wordpress/block-editor';

const SaveAccordion = ( { attributes } ) => {
	const { isOpenAll } = attributes;

	return (
		<div aria-multiselectable={ !isOpenAll }>
			<InnerBlocks.Content />
		</div>
	);
};

export default SaveAccordion;
