export default {
	isOpenAll: {
		type: 'bool',
		default: false,
	},
};
