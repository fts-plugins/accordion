export default {
	isOpen: {
		type: 'bool',
		default: false,
	},
};
