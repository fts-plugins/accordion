import { InnerBlocks } from '@wordpress/block-editor';

const SaveAccordionDetails = ( { attributes } ) => {
	const { isOpen } = attributes;

	return (
		<details open={ isOpen }>
			<InnerBlocks.Content />
		</details>
	);
};

export default SaveAccordionDetails;
