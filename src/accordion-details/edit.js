import { InspectorControls, InnerBlocks } from '@wordpress/block-editor';
import { PanelBody, PanelRow, ToggleControl } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

const EditAccordionItem = ( { attributes, setAttributes } ) => {
	const { isOpen } = attributes;
	const TEMPLATE = [
		[ 'fantassin/accordion-summary', { lock : { remove: true, move: true } } ],
		[ 'core/paragraph' ],
	];

	const handleCheckboxChange = ( isOpen ) => {
		setAttributes( { isOpen } );
	};

	return (
		<>
			<InspectorControls>
				<PanelBody
					title={ __( 'Options', 'fantassin-block-accordion' ) }
				>
					<PanelRow>
						<ToggleControl
							label={ __(
								'Is open by default',
								'fantassin-block-accordion'
							) }
							checked={ isOpen }
							onChange={ handleCheckboxChange }
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>

			<div
				style={ {
					backgroundColor: 'white',
					border: '1px solid #e0e0e0',
				} }
			>
				<InnerBlocks template={ TEMPLATE } />
			</div>
		</>
	);
};

export default EditAccordionItem;
