class FantassinAccordions {
  constructor(selector) {
    this.selector = selector

    this.init = this.init.bind(this)
  }

  init() {
    const accordionBlocks = Array.from(
      document.querySelectorAll(this.selector))
    accordionBlocks.forEach((accordionBlock) => {
      const options = {
        'multiselectable': accordionBlock.getAttribute('aria-multiselectable'),
      }

      new Details(accordionBlock, options).init()
    })
  }
}

class Details {
  constructor(parent, options) {
    this.parent = parent
    this.details = []
    this.options = options

    this.setListener = this.setListener.bind(this)
    this.toggleOnlyOne = this.toggleOnlyOne.bind(this)
  }

  init() {
    this.details = Array.from(this.parent.querySelectorAll('details'))
    this.setListener()
  }

  setListener() {
    if (this.options.multiselectable === 'false') {
      this.details.forEach((detail) => {
        detail.addEventListener('toggle', this.toggleOnlyOne);
      })
    }
  }

  toggleOnlyOne(e) {
    if (e.target.open) {
      this.details.forEach(detail => {
        if (detail !== e.target && detail.open) detail.open = false
      });
    }
  }
}

new FantassinAccordions('.wp-block-fantassin-accordion').init()
