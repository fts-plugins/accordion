<?php

namespace Fantassin\Block\Accordion;

use Fantassin\Core\WordPress\Contracts\BlockInterface;

class Accordion implements BlockInterface
{

    public function getName(): string
    {
        return 'fantassin/accordion';
    }
}
