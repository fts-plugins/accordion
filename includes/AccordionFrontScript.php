<?php

namespace Fantassin\Block\Accordion;

use Fantassin\Core\WordPress\Contracts\Hooks;

class AccordionFrontScript implements Hooks {

  public function hooks() {
    add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
  }

  public function enqueue_scripts() {
    wp_enqueue_script( 'fantassin-accordion-front', plugins_url( 'build/front/front.js', dirname( __FILE__ ) ), array(), '1.0.0', true );
  }
}
