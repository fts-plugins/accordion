<?php

namespace Fantassin\Block\Accordion;

use Fantassin\Core\WordPress\Blocks\BlockRegistry;
use Fantassin\Core\WordPress\Contracts\DynamicBlock;
use Fantassin\Core\WordPress\Contracts\Hooks;

class RegisterBlock implements Hooks {

	/** @var BlockRegistry */
	protected $registry;

	/** @var string */
	protected $pluginPath;


	/**
	 * RegisterBlocks constructor.
	 *
	 * @param BlockRegistry $blockRepository
	 */
	public function __construct( BlockRegistry $blockRepository, string $pluginDirectory ) {
		$this->registry   = $blockRepository;
		$this->pluginPath = $pluginDirectory;
	}

	public function hooks() {
		add_action( 'init', [ $this, 'register_blocks' ] );
	}

	public function register_blocks() {
		$repository = $this->getRegistry();
		foreach ( $repository->getBlocks() as $block ) {
			$this->registerBlock( $block );
		}
	}

	public function registerBlock( $block ) {
		$script_name = str_replace( '/', '-', $block->getName() );
		$block_name  = $block->getName();

		// $script_path = $this->pluginUrl . ;
		$script_asset_path = $this->pluginPath . '/build/index.asset.php';
		$script_path       = plugins_url( 'build/index.js', dirname( __FILE__ ) );
//		$script_asset_path = dirname( plugin_dir_path( __FILE__ ) ) . '/build/index.asset.php';

		$script_asset = file_exists( $script_asset_path )
			? require $script_asset_path
			: array(
				'dependencies' => array(),
				'version'      => null,
			);

		wp_register_script(
			$script_name,
			$script_path,
			$script_asset['dependencies'],
			$script_asset['version'],
			true
		);

		$isBlockRegistered = \WP_Block_Type_Registry::get_instance()->is_registered( $block_name );

		if ( $isBlockRegistered ) {
			return;
		}

		$args = [
			'editor_script' => $script_name,
		];

		if ( $block instanceof DynamicBlock ) {
			$args['render_callback'] = [ $block, 'renderBlock' ];
		}

		register_block_type( $block_name, $args );

		/* Translation */
		wp_set_script_translations(
			'fantassin-accordion-translations',
			'fantassin-accordion-block',
			dirname(plugin_dir_path( __FILE__ )) . DIRECTORY_SEPARATOR . 'languages'
		);
	}

	/**
	 * @return BlockRegistry
	 */
	public function getRegistry(): BlockRegistry {
		return $this->registry;
	}
}
