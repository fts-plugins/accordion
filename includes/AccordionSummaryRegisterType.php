<?php

namespace Fantassin\Block\Accordion;

use Fantassin\Core\WordPress\Contracts\Hooks;

class AccordionSummaryRegisterType implements Hooks {

	public function hooks() {
		add_action( 'init', [ $this, 'registerBlockType' ] );
	}

	public function registerBlockType() {
		register_block_type( 'fantassin/accordion-summary' );
	}

}
